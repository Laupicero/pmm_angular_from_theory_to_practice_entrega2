import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Para los formularios
import { ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { JokeComponent } from './Componentes/joke/joke.component';
import { JokelistComponent } from './Componentes/jokelist/jokelist.component';
import { JokeformComponent } from './Componentes/jokeform/jokeform.component';

@NgModule({
  declarations: [
    AppComponent,
    JokeComponent,
    JokelistComponent,
    JokeformComponent
  ],
  imports: [
    BrowserModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
