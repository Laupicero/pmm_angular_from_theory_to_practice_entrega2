import {Joke} from './Joke';

//Exportaremos esta variable a traves de nuestro servicio
export const JokeListDao: Joke[] = [
    new Joke(`Which machine turns coffee into code?`, `A Programmer`),
    new Joke(`The part of a computer that you can kick is...`, `The Hardware`),
    new Joke(`‘0’ is false and ‘1’ is true, right?`, `1`),
    new Joke(`A word use by programmers when... They do not want to explain what they did...`, `Algorithm`),
    new Joke(`How do you call a programmer from Findland?`, `‘Nerdic’`),
];