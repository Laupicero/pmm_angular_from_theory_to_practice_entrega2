// Esta clase será un plantilla
// Para nuestro Array de 'joke' en jokeList
// Como plantilla para joke, para crear nuevas bromas
export class Joke {
    public setup: String;
    public punchline: String;
    public hide: boolean;

    //Constructor
    constructor(setup: String, punchline: String) {
        this.setup = setup;
        this.punchline = punchline;
        this.hide = true;
      }

    //Método para ocultar ka broma
    toggle() {
        this.hide = !this.hide;
    }
}