import { TestBed } from '@angular/core/testing';

import { JokeServService } from './joke-serv.service';

describe('JokeServService', () => {
  let service: JokeServService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JokeServService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
