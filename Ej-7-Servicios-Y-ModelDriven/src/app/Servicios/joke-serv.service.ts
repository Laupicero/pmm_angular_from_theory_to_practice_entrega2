import { Injectable } from '@angular/core';
import {JokeListDao } from '../Models-Clases/JokesList_DAO';

@Injectable({
  providedIn: 'root'
})
export class JokeServService {

  constructor() { }

  //Método que nos devolverá la var/el array de bromas
  getArrayOfJokes(){
    return JokeListDao;
  }
}
