import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Joke} from './../../Models-Clases/Joke';

@Component({
  selector: 'joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.css']
})
export class JokeComponent{
  @Input('joke') data: Joke;
  @Output() jokeRemove = new EventEmitter<Joke>();

  //Borrar broma
  deleteJoke() {
    this.jokeRemove.emit(this.data);
  }

}
