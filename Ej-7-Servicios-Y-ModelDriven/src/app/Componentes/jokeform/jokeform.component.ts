import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Joke} from './../../Models-Clases/Joke';

@Component({
  selector: 'jokeform',
  templateUrl: './jokeform.component.html',
  styleUrls: ['./jokeform.component.css']
})
export class JokeformComponent{
  //Var para el form
  formCustomizableJoke: FormGroup;
  setup: FormControl;
  punchline: FormControl;

  //Var para las validaciones
  setupCorrectFormat: String;
  punchlineCorrectFormat: String;
  jokeFormat: Boolean;

  

  constructor(){
    this.setup = new FormControl('', Validators.required);
    this.punchline = new FormControl('', Validators.required);

    this.formCustomizableJoke = new FormGroup({
      punchlineForm: this.punchline,
      setupForm: this.setup
    });

    this.setupCorrectFormat = 'fondoBlancoError';
    this.punchlineCorrectFormat = 'fondoBlancoError';
    this.jokeFormat = true;
  }

  //var
  @Output() myOwnJoke = new EventEmitter<Joke>();


  //Evento que emitiremos a jokeList
  makeAJoke(setup: String, punchline: String){
    if(this.validateFormatJoke(setup, punchline)){
      this.myOwnJoke.emit(new Joke(setup, punchline));
    }
  }


  //Método que nos comprobará si la broma tiene el formato correcto
  validateFormatJoke(setup: String, punchline: String): Boolean{
    var response: boolean = true;

    if(setup.length < 6){
      alert(`your joke must have minimum '6' letters`);
      this.setupCorrectFormat = 'fondoRojoError';
      response = false;
    }else{
      this.setupCorrectFormat = 'fondoBlancoError';
    }

    if(punchline.length < 6){
      alert(`your joke-Answer must have minimum '6' letters`);
      this.punchlineCorrectFormat = 'fondoRojoError';
      response = false;
    }else{
      this.punchlineCorrectFormat = 'fondoBlancoError';
    }

    if(setup.length >= 6 && punchline.length >= 6){
      this.setupCorrectFormat = 'fondoBlancoError';
      this.punchlineCorrectFormat = 'fondoBlancoError';
      response = true;
    }

    return response;
  }

}
