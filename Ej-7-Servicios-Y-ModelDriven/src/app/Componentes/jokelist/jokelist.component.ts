import { Component} from '@angular/core';
import {Joke} from './../../Models-Clases/Joke';
import {JokeServService} from '../../Servicios/joke-serv.service';

@Component({
  selector: 'jokelist',
  templateUrl: './jokelist.component.html',
  styleUrls: ['./jokelist.component.css']
})
export class JokelistComponent{
  jokes: Joke[];

  //Inyectamos el servicio y le devolvemos el array
  constructor(private service: JokeServService) {
    this.jokes = this.service.getArrayOfJokes();
  }

  addJoke(joke) {
    this.jokes.unshift(joke);
    //this.jokes.push(joke);
  }

  removeJoke(joke) {
    let i = this.jokes.indexOf(joke);
    this.jokes.splice(i, 1);
  }

}
